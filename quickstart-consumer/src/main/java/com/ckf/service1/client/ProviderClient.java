package com.ckf.service1.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**@FeignClient 调用服务的名称  yml配置文件对应服务名
 * 使用@FeignClient 注解来标识你要调用生产者的服务名
 * @author serene
 * @date 2020/8/22 16:40
 */

@FeignClient(value = "quickstart-provider")
public interface ProviderClient {

    /**
     * 调用服务的方法
     * @return
     */
    @GetMapping("/service")
    public String service();
}
