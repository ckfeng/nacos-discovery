package com.ckf.service2.service;

import com.ckf.service1.service.ProviderService;

/**
 * service2服务 实现类
 * @author serene
 * @date 2020/8/24 17:52
 */

@org.apache.dubbo.config.annotation.Service
public class ProviderServiceImpl implements ProviderService {

    @Override
    public String service() {
        return "provider2 invoke";
    }
}
