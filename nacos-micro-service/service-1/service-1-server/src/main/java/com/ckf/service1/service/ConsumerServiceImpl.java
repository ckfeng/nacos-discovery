package com.ckf.service1.service;

/**Service 此类的方法将来要暴露为Dubbo接口
 * @author serene
 * @date 2020/8/24 17:20
 */

@org.apache.dubbo.config.annotation.Service   //注解标记此类的方法暴露为dubbo接口
public class ConsumerServiceImpl implements ConsumerService {

    /**
     * 注入service1 接口
     */
    @org.apache.dubbo.config.annotation.Reference
    ProviderService providerService;


    /**
     * Dubbo接口的内容
     */
    @Override
    public String service() {
        // service1 调用 service2
        String service = providerService.service();
        return "Consumer1 invoke" + service;
    }
}
