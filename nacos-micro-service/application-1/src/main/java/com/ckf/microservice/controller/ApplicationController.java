package com.ckf.microservice.controller;

import com.ckf.service1.service.ConsumerService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author serene
 * @date 2020/8/24 16:47
 */

@RestController
public class ApplicationController {

    /**
     * 注入service 基于dubbo协议
     * 生成接口代理对象，通过代理对象进行远程调用
     * @org.apache.dubbo.config.annotation.Reference
     */

    @Reference
    ConsumerService consumerService;

    @GetMapping("/service")
    public String service(){
        //远程调用
        String service = consumerService.service();
        return "test" + service;
    }
}
