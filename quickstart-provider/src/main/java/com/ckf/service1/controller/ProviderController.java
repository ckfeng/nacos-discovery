package com.ckf.service1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author serene
 * @date 2020/8/22 16:18
 */

@RestController
public class ProviderController {

    private static final Logger logger = LoggerFactory.getLogger(ProviderController.class);

    @GetMapping("/service")
    public String service(){
        logger.info("provider invoke");
        return "provider invoke";
    }
}
