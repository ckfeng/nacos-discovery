package com.ckf.service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @EnableDiscoveryClient 服务发现客户端
 * @EnableFeignClients feign客户端 进行远程调用
 * @author: serene
 * @date: 2020/8/022 16:26
 * @description:
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class ProviderApplication {

    public static void main(String[] args) {
        System.out.println("服务提供者启动成功");
        SpringApplication.run(ProviderApplication.class, args);
    }
}
