# nacos-discovery

#### 项目介绍
springcloud整合nacos实现服务的注册与发现以及网关的实现

网关的作用：过滤和路由转发。

quickstart-provider以及quickstart-consumer分别是服务的提供者以及消费者。

nacos-micro-service模块有两个服务service以及api测试还有网关（使他们通信）。


**博客地址为** :https://www.cnblogs.com/ckfeng/p/13551467.html

 **效果图如下：** 
![nacos的注册与发现效果图](https://images.gitee.com/uploads/images/2020/0823/235130_e42b1758_5420874.png "11.png")

 **多实例** 
![多实例](https://images.gitee.com/uploads/images/2020/0824/093742_e87d8bd5_5420874.png "22.png")

 **控制台** 
以轮回的方式打印

 **提供者服务1** 
![控制台1](https://images.gitee.com/uploads/images/2020/0824/094108_efb2a649_5420874.png "33.png")

 **提供者服务2** 
![控制台2](https://images.gitee.com/uploads/images/2020/0824/094004_50c9583a_5420874.png "44.png")

 **配置管理** 
![配置管理](https://images.gitee.com/uploads/images/2020/0825/094816_0adc9a28_5420874.png "22.png")

 **服务列表** 
![服务列表](https://images.gitee.com/uploads/images/2020/0825/094846_f1cb869a_5420874.png "服务列表.png")

 **访问地址** 
网关访问的地址一样，端口为56010
![访问地址](https://images.gitee.com/uploads/images/2020/0825/095127_8eb27e16_5420874.png "44.png")

 **压缩包那个是配置管理的配置文件** 

 **整体架构** 
![架构](https://images.gitee.com/uploads/images/2020/0825/095936_1d77dc6b_5420874.png "网关.png")